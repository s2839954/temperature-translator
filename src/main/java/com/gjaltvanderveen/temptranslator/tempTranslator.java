package com.gjaltvanderveen.temptranslator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "conversionServlet", value="/convert")
public class tempTranslator extends HttpServlet {
    public void init() throws ServletException {
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Book Quote";
        String converted = "";
        if (request.getParameter("type").equals("C°")) {
            converted = "F°";
        } else if (request.getParameter("type").equals("F°")) {
            converted = "C°";
        }
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>" +
                request.getParameter("type") +
                ": " +
                request.getParameter("temp") + "\n" +
                "  <P>" +
                converted +
                ": " +
                calculateTemp(Double.parseDouble(request.getParameter("temp")), request.getParameter("type")) +
                "</BODY></HTML>");
    }

    public double calculateTemp(double temp, String type) {
        if (type.equals("C°")) {
            return celToFahr(temp);
        } else if (type.equals("F°")) {
            return fahrToCel(temp);
        }
        return -300;
    }

    public double celToFahr(double celsius) {
        return celsius * 9 / 5 + 32;
    }

    public double fahrToCel(double fahrenheit) {
        return (fahrenheit -32) * 5 / 9;
    }
}
